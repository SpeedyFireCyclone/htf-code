//ts-check
const express = require('express')
const app = express()
const port = 3000

const beast = require('./routes/beast')
const equilibrium = require('./routes/equilibrium')
const goldeneagle = require('./routes/goldeneagle')
const morse = require('./routes/goldeneagle')
const pisano = require('./routes/pisano')

app.use(express.json());

app.get('/', (req, res) => res.send('Works!'))

app.post('/beast', beast)
app.post('/equilibrium', equilibrium)
app.post('/goldeneagle', goldeneagle)
app.post('/morse', morse)
app.post('/pisano', pisano)

app.listen(port, () => console.log(`Listening on port ${port}`))
