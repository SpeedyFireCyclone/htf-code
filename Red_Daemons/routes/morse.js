//ts-check
const morse = require('morse')

module.exports = function pistol(req, res) {

    let morse = toMorse(req.body.value);
    let decoded = decodeMorse(morse);

    res.json({
        "value": decoded
    })
}

function toMorse(bitstring) {

    const mod = longestSequence(bitstring)

    const pause = /0/g
    const dot = /1/g
    const dash = /111/g
    const charspace = /000/g
    const wordspace = /0000000/g

    let bitstream = bitstring.split('').filter((_, index) => (!(index % mod)))
    let filtered = bitstream.join('');

    let decoded = filtered.replace(wordspace, " ....... ")
    decoded = decoded.replace(dash, "-")
    decoded = decoded.replace(dot, ".")
    decoded = decoded.replace(charspace, " ")
    decoded = decoded.replace(pause, "");

    return decoded
}

function decodeMorse(code) {

    code = code.split(' ').filter((code) => (code != '----')).join(' ')

    let decode = morse.decode(code)
    return decode + "NULL"
}

function longestSequence(bitstring) {
    const re = /(0)\1+/g
    const matches = bitstring.match(re);

    matches.sort().reverse();

    let rate = (matches[0].length) / 7
    return rate
}