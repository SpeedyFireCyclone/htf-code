//ts-check
module.exports = function beast(req, res) {

    console.log(req.body);

    let offset = req.body.offset;
    const size = req.body.size;
    let array = []

    while (array.length != size) {
        if (array.length > 0) {
            offset = array[array.length - 1] + 1
        }
        array.push(parseInt(getPandigital(offset)))
    }

    res.json({
        "values": array
    })
}

function getPandigital(start) {

    //debugger;

    if (isPandigit(start)) {
        start += 1
    }

    let pandigit = start
    if (pandigit < 1023456789) {
        pandigit = 1023456789
    }

    pandigit = parseInt(pandigitString(pandigit))

    return pandigit
}

function pandigitString(start) {
    let offset = start.toString()
    let out = ""

    let options = [0, 1, 2, 3, 4, 5, 6, 7, 8, 9];
    let available = [0, 1, 1, 1, 1, 1, 1, 1, 1, 1];
    let tested = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0];

    while (out.length < 10) {
        if (out.length == 1) {
            available[0] = true;
        }
        let set = false;
        while (!set) {
            let first_available = available.findIndex((e, index) => (e && !tested[index]))
            tested[first_available] = true;
            let test = out + options[first_available]
            console.log(test);
            let against = offset.substring(0, test.length)
            if (parseInt(test) >= parseInt(against)) {
                out += options[first_available];
                available[first_available] = false;
                tested.fill(false, 0, 9);
                set = true;
            }
        }
    }

    return out;
}

function isPandigit(int) {
    let pdg = int.toString()
    let ispdg = true;
    let i = 0;
    while (i < 10 && ispdg) {
        ispdg = (pdg.lastIndexOf(i) != -1) && (pdg.indexOf(i) == pdg.lastIndexOf(i))
        i++
    }
    return ispdg;
}