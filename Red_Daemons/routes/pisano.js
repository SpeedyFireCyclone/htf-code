//ts-check
const gcd = require('greatest-common-divisor')

class Fraction {
    constructor(numerator, denominator) {

        while (numerator < 0) {
            numerator += denominator
        }

        let divisor = gcd(numerator, denominator)


        if (divisor > 1) {
            numerator = numerator / divisor
            denominator = denominator / divisor
        }

        this.numerator = numerator;
        this.denominator = denominator;
    }

    toString() {
        return `${this.numerator}/${this.denominator}`
    }
}

module.exports = function pisano(req, res) {

    let numerator = req.body.numerator;
    let denominator = req.body.denominator;

    let start = new Fraction(numerator, denominator)

    let fractions = fibonacci(start)

    res.json({
        "sumOfUnitFractions": fractions.join(' + ')
    })
}

function fibonacci(fraction) {
    let x = fraction.numerator;
    let y = fraction.denominator;

    let proper = new Fraction(1, Math.ceil(y / x))

    let factor_numerator = -y % x
    while (factor_numerator < 0) {
        factor_numerator += x
    }

    let factor = new Fraction(factor_numerator, y * (Math.ceil(y / x)))

    let retval = [proper]

    if (factor.numerator != 1) {
        return retval.concat(fibonacci(factor))
    } else {
        retval.push(factor)
        return retval
    }
}