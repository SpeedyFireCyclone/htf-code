//ts-check
module.exports = function goldeneagle(req, res) {
    const input = req.body.input;

    res.json({
        "solution": utils.encodeRoman(input)
    })
}

const utils = {
    encodeRoman: function (int) {
        let values = {
            M: 1000,
            D: 500,
            C: 100,
            L: 50,
            X: 10,
            V: 5,
            I: 1
        };
        let count = {
            M: 0,
            D: 0,
            C: 0,
            L: 0,
            X: 0,
            V: 0,
            I: 0
        };

        let roman = "";

        for (character in values) {
            count[character] = Math.floor(int / values[character]);
            int = int - (count[character] * values[character]);
            roman += utils.repeat(character, count[character])
        }

        return utils.replaceInvalid(utils.replaceInvalid(roman))
    },
    replaceInvalid: function (str) {
        const substitutions = [{
            invalid: "IIII",
            valid: "IV"
        },
        {
            invalid: "VIV",
            valid: "IX"
        },
        {
            invalid: "XXXX",
            valid: "XL"
        },
        {
            invalid: "LXL",
            valid: "XC"
        },
        {
            invalid: "CCCC",
            valid: "CD"
        }]

        let i = 0
        while (i < 5) {
            str = str.replace(substitutions[i].invalid, substitutions[i].valid)
            i++
        }

        return str
    },
    repeat: function (char, count) {
        let tmp = ""
        while (count > 0) {
            tmp += char
            count--
        }
        return tmp
    }
}