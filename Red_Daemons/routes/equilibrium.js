//ts-check
module.exports = function equilibrium(req, res) {
    
    console.log(req.body);

    const lowerb = req.body.value;
    let palindrome = lowerb;
    while (!utils.isPalindrome(palindrome.toString())){
        palindrome++
    }

    res.json({
        "value": palindrome
    })
}

const utils = {
    isPalindrome: function(str) {
        let palindrome = true
        let len = str.length;
        let i = 0;
        while(i < len && palindrome) {
            let a = str.charAt(i)
            let b = str.charAt(len-(i+1))
            palindrome = a === b;
            i++
        }
        return palindrome;
    }
}